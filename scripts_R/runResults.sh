#!/bin/bash

aSize=4

r="0.01 0.05 0.1"

alpha=0.95

t=16

p=12

echo r fitness > results4.csv

for i in ${r}
do
	echo -n $i' '
	./../build/demo ${aSize} ${i} ${t} ${p} ${alpha} >> results4.csv
done
echo

aSize=8

alpha=0.99

t=12

p=19

echo r fitness > results8.csv

for i in ${r}
do
	echo -n $i' '
	./../build/demo ${aSize} ${i} ${t} ${p} ${alpha} >> results8.csv
done
echo

aSize=16

alpha=0.99

t=9

p=17

echo r fitness > results16.csv

for i in ${r}
do
	echo -n $i' '
	./../build/demo ${aSize} ${i} ${t} ${p} ${alpha} >> results16.csv
done
echo

aSize=32

alpha=0.99

t=17

p=17

echo r fitness > results32.csv

for i in ${r}
do
	echo -n $i' '
	./../build/demo ${aSize} ${i} ${t} ${p} ${alpha} >> results32.csv
done
echo
