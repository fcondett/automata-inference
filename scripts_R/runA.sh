#!/bin/bash

aSize=32

r=0.1

nbRun=100

alphaMax=1

alpha="0.8 0.81 0.82 0.83 0.84 0.85 0.86 0.87 0.88 0.89 0.9 0.91 0.92 0.93 0.94 0.95 0.96 0.97 0.98 0.99"
t=12

p=19

echo a fitness > resultsAlpha32.csv

for i in ${alpha}
do
	echo -n $i' '
	./../build/demo ${aSize} ${r} ${t} ${p} ${i} >> resultsAlpha32.csv
done
echo
