#!/bin/bash

aSize=32

r=0.1

nbRun=100

alpha=0.99

tMax=20

p=19

echo t fitness > resultsTemperature4.csv

for((i=1;i <= ${tMax};i++))
do
	echo -n $i' '
	./../build/demo ${aSize} ${r} ${i} ${p} ${alpha} >> resultsTemperature4.csv
done
echo

aSize=8
echo t fitness > resultsTemperature8.csv

for((i=1;i <= ${tMax};i++))
do
	echo -n $i' '
	./../build/demo ${aSize} ${r} ${i} ${p} ${alpha} >> resultsTemperature8.csv
done
echo

aSize=16
echo t fitness > resultsTemperature16.csv

for((i=1;i <= ${tMax};i++))
do
	echo -n $i' '
	./../build/demo ${aSize} ${r} ${i} ${p} ${alpha} >> resultsTemperature16.csv
done
echo

aSize=32
echo t fitness > resultsTemperature32.csv

for((i=1;i <= ${tMax};i++))
do
	echo -n $i' '
	./../build/demo ${aSize} ${r} ${i} ${p} ${alpha} >> resultsTemperature32.csv
done
echo
