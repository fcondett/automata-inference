#!/bin/bash

aSize=4

r="0.01 0.05 0.1"

alpha=0.95

t=16

p=12

echo r fitness > resultsHC4.csv

for i in ${r}
do
	echo -n $i' '
	./../build/demo ${aSize} ${i} ${t} ${p} ${alpha} >> resultsHC4.csv
done
echo

aSize=8

alpha=0.99

t=12

p=19

echo r fitness > resultsHC8.csv

for i in ${r}
do
	echo -n $i' '
	./../build/demo ${aSize} ${i} ${t} ${p} ${alpha} >> resultsHC8.csv
done
echo

aSize=16

alpha=0.99

t=9

p=17

echo r fitness > resultsHC16.csv

for i in ${r}
do
	echo -n $i' '
	./../build/demo ${aSize} ${i} ${t} ${p} ${alpha} >> resultsHC16.csv
done
echo

aSize=32

alpha=0.99

t=17

p=17

echo r fitness > resultsHC32.csv

for i in ${r}
do
	echo -n $i' '
	./../build/demo ${aSize} ${i} ${t} ${p} ${alpha} >> resultsHC32.csv
done
echo
