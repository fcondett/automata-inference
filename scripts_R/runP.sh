#!/bin/bash

aSize=4

r=0.1

nbRun=100

alpha=0.99

t=12

pMax=20

echo p fitness > resultsPallier4.csv

for((i=1;i <= ${pMax};i++))
do
	echo -n $i' '
	./../build/demo ${aSize} ${r} ${t} ${i} ${alpha} >> resultsPallier4.csv
done
echo

aSize=8
echo p fitness > resultsPallier8.csv

for((i=1;i <= ${pMax};i++))
do
	echo -n $i' '
	./../build/demo ${aSize} ${r} ${t} ${i} ${alpha} >> resultsPallier8.csv
done
echo

aSize=16
echo p fitness > resultsPallier16.csv

for((i=1;i <= ${pMax};i++))
do
	echo -n $i' '
	./../build/demo ${aSize} ${r} ${t} ${i} ${alpha} >> resultsPallier16.csv
done
echo

aSize=32
echo p fitness > resultsPallier32.csv

for((i=1;i <= ${pMax};i++))
do
	echo -n $i' '
	./../build/demo ${aSize} ${r} ${t} ${i} ${alpha} >> resultsPallier32.csv
done
echo
