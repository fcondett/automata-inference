/*
  sample.h

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
 
 Date:
   2019/04/08 : Version 0
*/

#ifndef _sample_h
#define _sample_h

#include <iostream>
#include <fstream>
#include <vector>

#include "rapidjson/document.h"

#include <base/word.h>

/*
    Sample of words (vector of words)
*/
class Sample {
public:
    Sample() {
        // empty sample
    }

    Sample(const char * _instance_fileName) {
        std::ifstream filein(_instance_fileName);

        if (!filein)
            std::cerr << "Impossible to open " << _instance_fileName << std::endl;

        readFrom(filein);

        filein.close();
    }

    /*
        The size of the sample
    */
    unsigned size() const {
        return set.size();
    }

    /*
        Output
    */
    virtual void printOn(std::ostream& _os) const {
        _os << "{" ;

        _os << "\"sample\":[" ;
        if (set.size() > 0) {
            _os << set[0] ;
            for(unsigned i = 1; i < set.size(); i++)
                _os << "," << set[i] ;
        }
        _os << "]" ;

        _os << "}" ;
    }

    /*
        Input
    */
    virtual void readFrom(rapidjson::Value& document) {
        if (document.IsObject()) {
            if (document.HasMember("sample")) {
                for(const auto& word : document["sample"].GetArray()){
                    Word w;
                    w.readFrom(word);
                    // add the new word
                    set.push_back(w);
                }
            }  else
                std::cerr << "Error: sample not found." << std::endl;

        } else {
            std::cerr << "Sample::readFom: Impossible to parse the json stream." << std::endl;            
        }
    }

    virtual void readFrom(std::istream& _is) {
        std::string json((std::istreambuf_iterator<char>(_is)),
                 std::istreambuf_iterator<char>());

        rapidjson::Document document;

        document.Parse(json.c_str());

        if (document.IsObject()) {
            readFrom(document);
        } else {
            std::cerr << "DFA::readFrom: Impossible to parse the json istream " << std::endl;            
        }
    }

    virtual void add(Word & _w) {
        set.push_back(_w); 
    }

    // sample of words
    std::vector<Word> set;
};

std::ostream & operator<<(std::ostream& _os, const Sample& _s) {
    _s.printOn(_os);
    return _os;
}

std::istream & operator>>(std::istream& _is, Sample& _s) {
  _s.readFrom(_is);
  return _is;
}

#endif