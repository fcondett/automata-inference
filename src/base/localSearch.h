
#ifndef FSSP_LOCALSEARCH_H
#define FSSP_LOCALSEARCH_H
#include <base/solution.h>
#include <base/sample.h>

#include <utility>
#include "eval/eval.h"

using namespace std;

class LocalSearch {
public:
    virtual void run(Solution<double> &s) = 0;
};

class HillClimberBestImprov : public LocalSearch {
public:
    HillClimberBestImprov(Eval<double> &eval, int nEvalMax) : _eval(&eval) {
        _nEvalMax = nEvalMax;
        _nbUtil = 0;
    }

    void run(Solution<double> &s) {
        for (unsigned int i = 0; i < s.function.size(); i++) {
            for (unsigned int j = 0; j < s.function[i].size(); j++) {
                s.function[i][j] = (rand() % s.nStates + 1) - 1;
            }
        }
        _eval->operator()(s);
        _climb.push_back(s.fitness());
        _nbUtil = 1;
        int nbEval = 1;

        bool localOptimum = false;
        int oldState = -1;
        int iBest = -1;
        int jBest = -1;
        int stateBest = -1;
        double fitBest = -1;
        while (!localOptimum && nbEval < _nEvalMax) {

            double currentFit = s.fitness();
            for (unsigned int i = 0; i < s.function.size(); i++) {
                for (unsigned int j = 0; j < s.function[i].size(); j++) {
                    for(int k = 0; k < s.nStates; k++)
                    {
                        oldState = s.function[i][j];
                        s.function[i][j] = k;
                        _eval->operator()(s);
                        nbEval++;
                        s.function[i][j] = oldState;
                        if (fitBest < s.fitness()) {
                            fitBest = s.fitness();
                            iBest = i;
                            jBest = j;
                            stateBest = k;
                        }
                    }

                }
            }

            if (currentFit < fitBest) {
                s.function[iBest][jBest] = stateBest;
                s.fitness(fitBest);
                _climb.push_back(s.fitness());
                _nbUtil++;
            } else
                localOptimum = true;

        }

    }

    void print() {
        for (int i = 0; i < _nbUtil; i++)
            cout << _climb[i] << endl;
    }

protected:
    int _nEvalMax;
    int _nbUtil;
    vector<double> _climb;
    Eval<double> *_eval;
};


class RecuitSimule : public LocalSearch {
public:
    RecuitSimule(Eval<double> &eval, std::mt19937 gen, double T, int P, double alpha ) : _eval(&eval), _gen(gen) {
        _nbEval = 0;
        _T = T;
        _P = P;
        _alpha = alpha;
    }

    void run(Solution<double> &s) {
        std::uniform_int_distribution<int> distributionStates(-1, s.nStates -1);
        std::uniform_real_distribution<> distributionU(0, 1);
        for (unsigned int i = 0; i < s.function.size(); i++) {
            for (unsigned int j = 0; j < s.function[i].size(); j++) {
                s.function[i][j] = distributionStates(_gen);
            }
        }
        _eval->operator()(s);
        _recuit.push_back(s.fitness());
        _nbEval = 1;
        int nbUtil = 1;

        Solution<double> sBest = s;

        int oldState = -1;
        double delta = -1;
        int cpt_nochange = 0;
        std::uniform_int_distribution<int> distributionI(0, s.function.size() -1);
        while (cpt_nochange < (_P * 3) && sBest.fitness() < 1) {
            int i = distributionI(_gen);
            std::uniform_int_distribution<int> distributionJ(0, s.function[i].size() -1);
            int j = distributionJ(_gen);
            int k = distributionStates(_gen);
            oldState = s.function[i][j];
            s.function[i][j] = k;
            _eval->operator()(s);
            double currentFit = s.fitness();
            s.function[i][j] = oldState;
            _eval->operator()(s);

            delta = currentFit - s.fitness();
            if (delta > 0) {
                s.fitness(currentFit);
                _recuit.push_back(currentFit);
                s.function[i][j] = k;
                cpt_nochange = 0;
                _nbEval++;
            } else {
                double u = distributionU(_gen);
                if (u < exp((delta / _T))) {
                    s.fitness(currentFit);
                    _recuit.push_back(currentFit);
                    s.function[i][j] = k;
                    cpt_nochange = 0;
                    _nbEval++;
                } else {
                    cpt_nochange++;
                }
            }

            nbUtil++;
            if(nbUtil % _P == 0 ) {
                _T *= _alpha;
            }

            if(s.fitness() > sBest.fitness())
                sBest = s;
            
        }

        s = sBest;
    }

    void print() {
        for (int i = 0; i < _nbEval; i++)
            cout << _recuit[i] << endl;
    }

protected:
    int _nbEval;
    double _T;
    int _P;
    double _alpha;
    vector<double> _recuit;
    Eval<double> *_eval;
    std::mt19937 _gen;
};

#endif //FSSP_LOCALSEARCH_H
