/*
  demo.cpp

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
 
 Date:
   2019/09/03 : Version 0

***
    Demo of candidate solution evaluation 

*** To compile from automata-inference directory:

mkdir build
cd build
cmake ../src/exe
make


*** To run:
./demo

*/
#include <iostream>
#include <sstream>
#include <fstream>
#include <random>

#include <base/sample.h>
#include <base/solution.h>
#include <base/localSearch.h>

#include <eval/basicEval.h>
#include <eval/smartEval.h>
#include <eval/basicBiobjEval.h>
#include <eval/smartBiobjEval.h>

using namespace std;


typedef pair<double, unsigned> Fitness2;

/* 
    Main
*/
int main(int argc, char ** argv) {
    int seed = 0;
    float avgFit = 0;
    vector<float> fitPlot;
    string r = argv[2];
    int n = atoi(argv[1]);
    for(seed = 0; seed <= 29; seed++) {

        string filename_train = "../instances/dfa_" + to_string(n) + "_" +
                to_string(seed) + "_" + r + "_train-sample.json";

        std::istringstream ss(filename_train);
        std::string token;
        vector<string> vars;

        while (std::getline(ss, token, '_')) {
            vars.push_back(token);
        }

        Sample sample(filename_train.c_str());

        // Candidate solution with 5n/4 states, 2 digits, and a fitness value which is correct classification rate
        Solution<double> x((5 * stoi(vars[1]))/ 4, 2);

        // Evaluation : transition function is taken into account. Acceptance states are optimal ones.
        //              fitness is the classification rate

        // random generator with random seed
        std::mt19937 gen(stoi(vars[2]));

        SmartEval seval(gen, sample);

        /*HillClimberBestImprov hillClimb(seval, 100000);

        hillClimb.run(x);*/

        RecuitSimule recuitSimule(seval, gen, stod(argv[3]), stoi(argv[4]), stod(argv[5]));
        recuitSimule.run(x);


        string filename_test = vars[0] + "_" + vars[1] + "_" + vars[2] + "_" + vars[3] + "_test-sample.json";
        Sample sample2(filename_test.c_str());
        BasicEval beval(sample2);

        beval(x);

        avgFit += x.fitness();
        fitPlot.push_back(x.fitness());
    }

    avgFit /= (double)seed;
    //cout<<"final fitness: "<<endl;
    //cout<<stod(argv[3])<<" "<<avgFit<<endl;
    for(float f : fitPlot) {
        cout<<stof(r)<<" "<<f<<endl;
    }

    return 0;
}